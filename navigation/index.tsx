import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';
import { Header, TitleApp, Icons } from './styles';
import { View } from '../components/Themed';

import NotFoundScreen from '../screens/NotFoundScreen';
import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import { Octicons, Entypo } from '@expo/vector-icons';

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar style="light" backgroundColor="#054d45" />
      <Header>
        <TitleApp>Whatsapp</TitleApp>
        <Icons>
          <Octicons
            name="search"
            color="#fff"
            size={20} />
          <Entypo
            name="dots-three-vertical"
            color="#fff"
            size={20}
            style={{marginLeft: 22}} />
        </Icons>
      </Header>

      <Stack.Navigator screenOptions={{
        headerShown: false,
        headerTitleStyle: {
          color: '#fdfbfa',
        },
        headerStyle: {
          backgroundColor: '#085e56',
          borderBottomWidth: 0,
          elevation: 0
        }
      }}
      >
        <Stack.Screen name="Root" component={BottomTabNavigator} />
        <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      </Stack.Navigator>
    </View>

  );
}
