import { Ionicons, Fontisto } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Text, View } from 'react-native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import TabOneScreen from '../../screens/TabOneScreen';
import TabTwoScreen from '../../screens/TabTwoScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../../types';
import { Link, LinkText, Number, LinkSignal } from './styles';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
   
    const colorScheme = useColorScheme();

    return (
        <BottomTab.Navigator
            initialRouteName="TabOne"
            tabBarOptions={{
                adaptive: false,
                activeTintColor: '#ffffff',
                inactiveTintColor: '#b1ccc9',

                tabStyle: {
                    marginLeft: 10,
                },
                iconStyle: {

                },
                style: {
                    padding: 0,
                    margin: 0,
                    alignItems: 'center',
                    position: 'absolute',
                    top: 0,
                    backgroundColor: '#065e55',
                    borderTopWidth: 0,
                    elevation: 2,                    
                }
            }}
            screenOptions={{
            
            }}
        >
            <BottomTab.Screen
                name="Camera"
                component={TabOneNavigator}
                options={{
                    title: "",
                    tabBarIcon: ({ color }) => (
                        <Link color={color}>
                            <Fontisto name="camera" color={color} size={19} />
                        </Link>
                    )
                }}
            />

            <BottomTab.Screen
                name="TabOne"
                component={TabOneNavigator}
                options={{
                    title: "",
                    tabBarIcon: ({ color }) => (
                        <Link color={color} style={{marginLeft: -58, width: '165%'}}> 
                            <LinkText color={color}>CONVERSAS</LinkText>
                            <Number color={color}>24</Number>
                        </Link>),
                }}
            />
            <BottomTab.Screen
                name="TabTwo"
                component={TabTwoNavigator}
                options={{
                    title: "",
                    tabBarIcon: ({ color }) => (
                        <Link color={color} style={{marginLeft: -6 }}>
                            <LinkText color={color}>STATUS</LinkText>
                            <LinkSignal color={color}/>
                        </Link>),
                }}
            />
            <BottomTab.Screen
                name="TabThre"
                component={TabTwoNavigator}
                options={{
                    title: '',
                    tabBarIcon: ({ color }) => (
                        <Link color={color} style={{marginLeft: -8 }}>
                            <LinkText color={color}>CHAMADAS</LinkText>
                        </Link>)
                }}
            />
        </BottomTab.Navigator>
    );
}


// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
    return (
        <TabOneStack.Navigator>
            <TabOneStack.Screen
                name="TabOneScreen"
                component={TabOneScreen}
                options={{ headerTitle: 'Tab One Title', headerShown: false }}
            />
        </TabOneStack.Navigator>
    );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
    return (
        <TabTwoStack.Navigator>
            <TabTwoStack.Screen
                name="TabTwoScreen"
                component={TabTwoScreen}
                options={{ headerTitle: 'Tab Two Title', headerShown: false }}
            />
        </TabTwoStack.Navigator>
    );
}
