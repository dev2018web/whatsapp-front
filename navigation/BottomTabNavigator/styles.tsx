import styled from 'styled-components';
import { Text, View } from 'react-native';

export const Link:any = styled(View)`
    height: 60px;
    padding-top: 20px;
    align-items: center;
    justify-content: center;
    margin-bottom: -4px;
    border-bottom-width: ${(props:any) => props?.color === '#ffffff' ? '3px' : '0'};
    flex-direction: row;
    border-bottom-color:#e4efef;
    align-self: flex-start;
`;
export const LinkText:any = styled(Text)`
    color: ${(props:any) => props?.color};
    font-family: Roboto_700Bold;
`;

export const LinkSignal = styled(View)`
    width: 5px;
    height: 5px;
    border-radius: 5px;
    margin-left: 5px;
    background-color: ${(props:any) => props?.color};
`;

export const Number:any = styled(Text)`
    color:#075e55; 
    margin-left: 4px;
    background-color: ${(props:any) => props?.color};
    width: 19px;
    height: 19px;
    text-align: center;
    font-size: 12px;
    border-radius: 10px;
`;
