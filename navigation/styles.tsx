import styled from 'styled-components';
import { Text, View } from 'react-native';
import Constants from "expo-constants";

export const Header:any = styled(View)`
    margin-top: ${Constants.statusBarHeight};
    height: 56px;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    background-color: #085e56;
    justify-content: space-between;
    padding: 0 10px;
`;


export const TitleApp:any = styled(Text)`
    font-family: Roboto_500Medium;
    color: #fff;
    font-size: 20px;
`;


export const Icons:any = styled(View)`
    flex-direction: row;
`;

// export const LinkSignal = styled(View)`
//     width: 5px;
//     height: 5px;
//     border-radius: 5px;
//     margin-left: 5px;
//     background-color: ${(props:any) => props?.color};
// `;

// export const Number:any = styled(Text)`
//     color:#075e55; 
//     margin-left: 4px;
//     background-color: ${(props:any) => props?.color};
//     width: 19px;
//     height: 19px;
//     text-align: center;
//     font-size: 12px;
//     border-radius: 10px;
// `;
