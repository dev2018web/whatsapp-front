import styled from 'styled-components';
import { ImageBackground, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export const Contacts: any = styled(ScrollView)`
    margin-top: 50px;
    background-color: #fafafa;
`;

export const Contact: any = styled(View)`
    flex-direction: row;
    align-items: center;
    padding: 10.5px 12px;
`;

export const ContPhoto = styled(View)`
    width: 56px;
    height: 56px;
    border-radius: 50px;
    overflow: hidden;
`;

export const ImagePhoto = styled(ImageBackground)`
    width: 100%;
    height: 100%;
`;

export const BoxLeft = styled(View)`
    justify-content: space-between;
    flex-direction: row;
    flex: 1;
    border-bottom-color: #dadada;
    border-bottom-width: 0.2px;
    padding-bottom: 15px;
    margin-bottom: -15px;
    margin-left: 14px;
`;

export const ContName = styled(Text)`
    font-family: Roboto_700Bold;
    margin-bottom: 1px;
    font-size: 16px;
`;

export const ShortMessage: any = styled(Text)`
    color: #737373;
`;

export const ContContent: any = styled(View)`
`;

export const ContInfoMessage: any = styled(View)`
    align-items: center;
`;

export const DataMessage = styled(Text)`
    color: #20c253;
    font-family: Roboto_400Regular;
    font-size: 13px;
    margin-bottom: 3px;
`;

export const NumberMessage: any = styled(Text)`
    background-color: #20c253;
    width: 22px;
    height: 22px;
    text-align: center;
    border-radius: 50px;
    color: #f6fcf8;
    font-size: 12px;
    align-items: center;
    justify-content: center;
    line-height: 20px;
    font-family: Roboto_400Regular;

`;