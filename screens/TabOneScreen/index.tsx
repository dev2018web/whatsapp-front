import * as React from 'react';
import { Contact, Contacts, ContPhoto, ImagePhoto, ContName, ShortMessage, ContContent, ContInfoMessage, NumberMessage, DataMessage, BoxLeft } from './styles';

export default function TabOneScreen() {
    return (
        <Contacts>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Paul...</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>1</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_2.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Kunal</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:23</DataMessage>
                        <NumberMessage>5</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_3.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Mark</ContName>
                        <ShortMessage>When?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:22</DataMessage>
                        <NumberMessage>3</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_4.webp')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Rahul</ContName>
                        <ShortMessage>Confirmed</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:07</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_5.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Elen Vizinha</ContName>
                        <ShortMessage>Tua encomenda chegou</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:01</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_6.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Vacation</ContName>
                        <ShortMessage>Hey</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>07:45</DataMessage>
                        <NumberMessage>1</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
            <Contact>
                <ContPhoto>
                    <ImagePhoto source={require('../../assets/images/profile/image_1.jpg')} />
                </ContPhoto>
                <BoxLeft>
                    <ContContent>
                        <ContName>Jehn Y.</ContName>
                        <ShortMessage>Hi! Are you there?</ShortMessage>
                    </ContContent>
                    <ContInfoMessage>
                        <DataMessage>08:27</DataMessage>
                        <NumberMessage>2</NumberMessage>
                    </ContInfoMessage>
                </BoxLeft>
            </Contact>
      
        </Contacts>
    );
}
